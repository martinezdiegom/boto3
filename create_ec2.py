import boto3
client = boto3.client('ec2')
# create ec2 t2 micro instance with Amazon linux 2 AMI
response = client.run_instances(ImageId='ami-09d95fab7fff3776c',
                     InstanceType='t2.micro',
                     MinCount=1,
                     MaxCount=1)
#print InstanceId of ec2 created 
for instance in response ['Instances']:
    print (instance['InstanceId'])
#print all ec2 instance attributes
print(response)
    
